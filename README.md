# miarcade

Forked from [ckau-book (RUS)](https://github.com/CkauNui/ckau-book).

Theme for Batocera >5.24 and more

Theme name:     miarcade

Author:         Ckau, PCQ

Site: https://miarcade.com

Based:      ckau-book, Alekfull Bello v2, Art Book, Carbon, Colorful

Language: Spanish, Russian, English, Deutsch, French, Polish

`https://gitlab.com/miarcade/miarcade-bt`

## Changelog:

- 23 may 2022: Changed loading screen.
- 6 nov 2022: Added versions.

---

DISCLAIMER
----------

This theme is a customization for miarcade.com.

This theme for Batocera Linux is free (as in free beer). See `LICENSE` file.

Please note that Batocera itself is a set of other projects that are free (as in free speech) and open source. See `https://wiki.batocera.org/license`.

If you receive this theme in a bundle with Batocera or other related systems, be sure that you have been not charged for the software itself.

Logos and other artwork used are copyright of their respective owners.
